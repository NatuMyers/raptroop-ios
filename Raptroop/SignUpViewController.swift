//  SignUpViewController.swift
//  Raptroop
//
//  Created by Natu Myers on 2016-02-19.
//  Copyright © 2016 Altermax. All rights reserved.
//

import Alamofire
import SwiftyJSON
import UIKit

class SignUpViewController: UIViewController {
    var onButtonTapped : (() -> Void)? = nil
    
    @IBOutlet weak var usernametextfield: UITextField!
    
    @IBAction func continueButtonTap(sender: AnyObject) {
        
        print("continuepress");
        digestUser()
        
        // IF NO ERROR W/ SQL VALIDATION
        performSegueWithIdentifier("SegueAfterJoin", sender: self)

        
    }
    
    @IBOutlet weak var passwordtextfield: UITextField!
    @IBOutlet weak var emailtextfield: UITextField!
    @IBOutlet weak var loginMessage: UILabel!
    
    var inputboxes: [String:AnyObject] = [:]
    lazy var json : JSON = JSON.null
    
    // MUST HAVE HTTPS
    let endpoint = "https://troop.tech/api/users"
    
    
    
    func digestUser() {
        
        let passwordInput = self.passwordtextfield.text
        let usernameInput = self.usernametextfield.text
        let emailInput = self.emailtextfield.text
        
        
        inputboxes = [
            "hashword": self.passwordtextfield.text!,
            "username": self.usernametextfield.text!,
            "email": self.emailtextfield.text!
        ]
        
        // POST requests dont need a response!
        Alamofire.request(.POST, endpoint, parameters: inputboxes)
            .responseJSON { response in
                if let JSON = response.result.value {
                    print("Success with JSON: \(JSON)")
                    
                }
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        if (segue.identifier == "SegueAfterJoin") {
            
            var nextViewCont = segue!.destinationViewController as! SecondSignUpViewController;
            
            // PASS VARS FROM THIS VIEW TO NEXT
            nextViewCont.passedPassword_toView2 = self.passwordtextfield.text
            nextViewCont.passedEmail_toView2 =  self.emailtextfield.text
            nextViewCont.passedUsername_toView2 =  self.usernametextfield.text

            
        }
    }
    
}