//
//  MapViewController.swift
//  Raptroop
//
//  Created by Natu Myers on 2016-02-15.
//  Copyright © 2016 Altermax. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire

class MapViewController: UIViewController {
    
    // 1. LOAD IN SENT VARS (so that it exists in this view) (at top or in viewdidload?...)
    var passedPassword_toViewDashboard:String!
    var passedUsername_toViewDashboard:String!
    var passedEmail_toViewDashboard:String!
    var passedFocus_toViewDashboard:String!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let camera = GMSCameraPosition.cameraWithLatitude(-33.86,
            longitude: 151.20, zoom: 6)
        let mapView = GMSMapView.mapWithFrame(CGRectZero, camera: camera)
        mapView.myLocationEnabled = true
        self.view = mapView
        
        
        // 1. REQUEST (no params now)                           //2. HANDLE RESPONSE
        Alamofire.request(.GET, "https://troop.tech/api/users").responseJSON { (response) -> Void in
            
            //3. BINDING
            if let JSON = response.result.value {
                // print(JSON) //print whole json
                print(JSON)
            }
        }
        
        
        
        // MARKERS ON THE MAP
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(-33.86, 151.20)
        marker.title = "Sydney"
        marker.snippet = "Australia"
        marker.map = mapView


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
