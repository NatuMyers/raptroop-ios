//
//  DashboardNavViewController.swift
//  Raptroop
//
//  Created by Natu Myers on 2016-03-22.
//  Copyright © 2016 Altermax. All rights reserved.
//

// What makes a navigation controller special is that it creates and manages a hierarchy of view controllers, often referred to as a navigation stack. http://code.tutsplus.com/tutorials/ios-from-scratch-with-swift-navigation-controllers-and-view-controller-hierarchies--cms-25462

import Foundation
import UIKit

class DashboardNavViewController: UINavigationController {
    
    // 1. LOAD IN SENT VARS (so that it exists in this view) (at top it seems oposed to in viewdidload?...)
    var passedPassword_toViewDashboard:String!
    var passedUsername_toViewDashboard:String!
    var passedEmail_toViewDashboard:String!
    var passedFocus_toViewDashboard:String!




}
