//
//
//  Raptroop
//
//  Created by Natu Myers on 2016-03-21.
//  Copyright © 2016 Altermax. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON



class SecondSignUpViewController: UIViewController {
    var onButtonTapped : (() -> Void)? = nil
    
    // user's focus to be sent to next view contr.
    var selectedFocus = String()
    
    // 1. LOAD IN SENT VARS (so that it exists in this view) (at top it seems oposed to in viewdidload?...)
    var passedPassword_toView2:String!
    var passedUsername_toView2:String!
    var passedEmail_toView2:String!


    
    @IBAction func continueButtonPressed2(sender: AnyObject) {
        performSegueWithIdentifier("SegueAfterJoin2", sender: self)
    }
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var displayedImage: UIImageView!
    @IBOutlet weak var imageShown: UIImageView!
    
    let image0 = UIImage(named: "music")
    let image1 = UIImage(named: "technology")
    
    
    @IBAction func indexChanged(sender: AnyObject) {
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            print("case0")
            textLabel.text = "You are a music artist who needs services.";
            imageShown.image = image0
            selectedFocus = "artist"
        case 1:
            print("case1")
            textLabel.text = "You want to share your talent with music artists.";
            imageShown.image = image1
            selectedFocus = "provider"
        default:
            break; 
        }
    }
    //@IBOutlet weak var typeChooser: SecondSignUpViewController!
    //@IBOutlet weak var descriptionLabel: UILabel!

    
    
    // VAR SHIFT 
    
    // GET FROM PREVIEW VIEW
    override func viewDidLoad() {
        super.viewDidLoad()
        print("view 2 loaded")

        // 2. (for ease,) TRANSFER TO VAR NAMES
        var myLocalPassword = passedPassword_toView2
        var myLocalUsername = passedUsername_toView2
        var myLocalEmail = passedEmail_toView2

    }
    
    // 3. SEND VARS TO NEW VIEW
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        if (segue.identifier == "SegueAfterJoin2") {
           // NOTE! THIS IS TWEAKED with "as UINavigationController"
            var nextViewCont = segue.destinationViewController as! DashboardNavViewController
            
            // PASS VARS FROM THIS VIEW TO NEXT
            nextViewCont.passedPassword_toViewDashboard = passedPassword_toView2
            nextViewCont.passedEmail_toViewDashboard = passedUsername_toView2
            nextViewCont.passedUsername_toViewDashboard = passedEmail_toView2
            
            //toss this in too!
            nextViewCont.passedFocus_toViewDashboard = selectedFocus
            
        }
    }
    
    // END VAR SHIFT
    
   
    
}