//
//  MatchUpViewController.swift
//  Raptroop
//
//  Created by Natu Myers on 2016-03-22.
//  Copyright © 2016 Altermax. All rights reserved.
//

import Foundation
import UIKit




class MatchUpViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let draggableBackground: DraggableViewBackground = DraggableViewBackground(frame: self.view.frame)
        self.view.addSubview(draggableBackground)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
